<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appt extends Model
{
    public $timestamps = false;
    protected $fillable = ['email', 'start_date', 'start_hour'];
}
