<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ApptCollection;
use App\Appt;
use Carbon\Carbon;

class ApptController extends Controller
{
    public function store(Request $request)
    {
        //Let's check office hour
        if (!$this->is_valid_hour($request->get('start_hour'))){
            return response()->json('Not office hours');
            die;
        }

        //Let's check office date
        if (!$this->is_valid_date($request->get('start_date'))){
            return response()->json('Appointments cannot be in the weekend');
            die;
        }


        if (!($this->find_email($request->get('email')))){

            if ($this->is_day_and_hour_free($request->get('start_date'), $request->get('start_hour'))){
                $mydate = $request->get('start_date');
                $parsed_date = Carbon::parse($mydate)->toDateTimeString();

                $appt = new Appt([
                    'email' => $request->get('email'),
                    'start_date' => $parsed_date,
                    'start_hour' => $request->get('start_hour')
                    ]);
                $appt->save();
                return response()->json('success');
            } else {
                return response()->json('This Hour is already taken');
            }

        } else {
            return response()->json('This email already has an appointment');
        }

    }

    public function index()
    {
      return new ApptCollection(Appt::all());
    }

    public function edit($id)
    {
      $appt = Appt::find($id);
      return response()->json($appt);
    }

    public function update($id, Request $request)
    {
      $appt = Appt::find($id);

      $appt->update($request->all());

      return response()->json('successfully updated');
    }

    public function delete($id)
    {
      $appt = Appt::find($id);

      $appt->delete();

      return response()->json('successfully deleted');
    }

    private function find_email($email){
        $appt_num = Appt::where('email', $email)->get()->count();
        if ($appt_num > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function is_day_and_hour_free($start_date, $start_hour){

        $results_num = Appt::where('start_date', $start_date)
                        ->where('start_hour', $start_hour)->get()->count();
        
        if ($results_num === 0){
            return true;
        } else {
            return false;
        }
    }

    private function is_valid_hour($start_hour){
        if (($start_hour >= 9) && ($start_hour <= 18)){
            return true;
        } else {
            return false;
        }
    }

    private function is_valid_date($start_date){

        $weekendDay = false;
        
        $day = date("D", strtotime($start_date));
        
        if($day == 'Sat' || $day == 'Sun'){
            $weekendDay = true;
        }
        
        if($weekendDay){
            return false;
        } else{
            return true;
        }
    }

    public function fetch_available_hours($start_date, Request $request){
        $available_hours_default = [0,9,10,11,12,13,14,15,16,17,18];
        $results = Appt::where('start_date', $start_date)->get();
        $used_hours = array();
        $used_hours[] = 0;

        foreach ($results as $result){ 
            $used_hours[] = $result['start_hour'];
        }

        $available_hours = array_diff($available_hours_default, $used_hours);
        return response()->json($available_hours); 
    }

}