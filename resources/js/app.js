require('./bootstrap');
require('vue-flash-message/dist/vue-flash-message.min.css');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import VueFlashMessage from 'vue-flash-message';
Vue.use(VueFlashMessage);

import HomeComponent from './components/HomeComponent.vue';
import CreateComponent from './components/CreateComponent.vue';
import IndexComponent from './components/IndexComponent.vue';
import EditComponent from './components/EditComponent.vue';

const routes = [
  {
      name: 'home',
      path: '/',
      component: CreateComponent
  },
  {
    name: 'appts',
    path: '/appts',
    component: IndexComponent
  },
  {
      name: 'create',
      path: '/create',
      component: CreateComponent
  },
  {
      name: 'edit',
      path: '/edit/:id',
      component: EditComponent
  }

];

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(
    Vue.util.extend(
        { router }, App)
    ).$mount('#app');