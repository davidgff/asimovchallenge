<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/appt/create', 'ApptController@store');
Route::get('/appt/edit/{id}', 'ApptController@edit');
Route::post('/appt/update/{id}', 'ApptController@update');
Route::delete('/appt/delete/{id}', 'ApptController@delete');
Route::get('/appt/fetch/{start_date}', 'ApptController@fetch_available_hours');
Route::get('/appts', 'ApptController@index');
